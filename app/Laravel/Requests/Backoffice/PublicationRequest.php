<?php namespace App\Laravel\Requests\Backoffice;

use Session,Auth, Input;
use App\Laravel\Requests\RequestManager;

class PublicationRequest extends RequestManager{

	public function rules(){

		$rules = [
			'title' => "required",
			'description' => "required",
			'file' => "required",
		];

		return $rules;
	}

	public function messages(){
		return [
			'required' => "This field is required.",
		];
	}
}