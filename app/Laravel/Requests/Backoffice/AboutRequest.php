<?php namespace App\Laravel\Requests\Backoffice;

use Session,Auth, Input;
use App\Laravel\Requests\RequestManager;

class AboutRequest extends RequestManager{

	public function rules(){

		$rules = [
			'about' => "required",
			'address' => "required",
			'email' => "required",
			'contact' => "required",
		];

		return $rules;
	}

	public function messages(){
		return [
			'required' => "This field is required.",
		];
	}
}