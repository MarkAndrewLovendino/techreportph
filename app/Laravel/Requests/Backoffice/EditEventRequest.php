<?php namespace App\Laravel\Requests\Backoffice;

use Session,Auth, Input;
use App\Laravel\Requests\RequestManager;

class EditEventRequest extends RequestManager{

	public function rules(){

		$rules = [
			'title' => "required",
			'excerpt' => "required",
			'details' => "required",
			'address' => "required",
			'daterange' => "required",
			'file' => "image",
		];

		return $rules;
	}

	public function messages(){
		return [
			'required' => "This field is required.",
		];
	}
}