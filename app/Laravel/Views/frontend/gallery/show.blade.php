@extends('frontend._layout.main')

@section('content')
<div class="clearfix"></div>
      <section class="my-breadcrumb parallex">
         <div class="container page-banner">
            <div class="row">
               <div class="col-md-12 col-sm-12 col-xs-12 heading">
                  <div class="main-heading-container">
                     <h1 style="font-size: 50px;"><b>GALLERY</b></h1><br>
                     <h4><i>White Hat is powerful, beautiful, and fully responsive WordPress Theme with multiple options</i></h4>
                  </div>
               </div>
            </div>
         </div>
      </section>

<div id="page-content" class="page-wrapper">
    <div class="zm-section single-post-wrap bg-white ptb-20 xs-pt-30">
        <div class="container">
            <div class="col-md-12">
                <div class="row">
                    <h1 style="padding-bottom: 20px; padding-top: 20px;"><center><b>{{$album->title}}</b></center></h1>
                    <div class="gal">
                        @forelse($album->news as $index => $info)
                        <img src="{{asset($info->directory.'/'.$info->filename)}}" alt="">
                        @empty
                        No Photos uploaded yet
                        @endforelse

                    </div>
                </div>
            </div>

            <div class="pager">
                {!!$images->appends(Input::except('page'))->render()!!}
                    {{-- <ul class="pagination style2">
                        <li><a href="#"><i class="fa fa-chevron-left"></i></a></li>
                        <li class="selected"><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li><a href="#">5</a></li>
                        <li><a href="#">6</a></li>
                        <li><a href="#"><i class="fa fa-chevron-right"></i></a></li>
                        </ul> --}}
            </div>
        </div>
    </div>
</div>

@stop

@section('page-styles')
<style type="text/css">
.gal {


    -webkit-column-count: 3; /* Chrome, Safari, Opera */
    -moz-column-count: 3; /* Firefox */
    column-count: 3;


}   
.gal img{ width: 100%; padding: 7px 0;}
@media (max-width: 500px) {

    .gal {


        -webkit-column-count: 1; /* Chrome, Safari, Opera */
        -moz-column-count: 1; /* Firefox */
        column-count: 1;


    }

}
</style>
@stop

@section('page-scripts')
@stop