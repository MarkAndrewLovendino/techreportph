@extends('frontend._layout.main')

@section('content')
<div class="clearfix"></div>
      <section class="my-breadcrumb parallex">
         <div class="container page-banner">
            <div class="row">
               <div class="col-md-12 col-sm-12 col-xs-12 heading">
                  <div class="main-heading-container">
                     <h1 style="font-size: 50px;"><b>GALLERY</b></h1><br>
                     <h4><i>White Hat is powerful, beautiful, and fully responsive WordPress Theme with multiple options</i></h4>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <section class="project-container-2" style="background-color: #D0E4F9">
         <div class="container">
            <div class="row">
               @foreach($albums as $index => $info)
               <div class="col-md-4 col-sm-6 col-xs-12">
                  <div class="recent-project">
                     <a href="{{route('frontend.gallery.show',$info->id.'-'.Str::slug($info->title))}}"><img src="{{asset($info->directory.'/'.$info->filename)}}" alt=""></a>
                     <div class="project-info">
                        <h3><a href="{{route('frontend.gallery.show',$info->id.'-'.Str::slug($info->title))}}">{{$info->title}}</a></h3>
                     </div>
                  </div>
               </div>
               @endforeach
            </div>

            <div class="pager">
              {!!$albums->appends(Input::except('page'))->render()!!}
              {{-- <ul class="pagination style2">
                      <li><a href="#"><i class="fa fa-chevron-left"></i></a></li>
                      <li class="selected"><a href="#">1</a></li>
                      <li><a href="#">2</a></li>
                      <li><a href="#">3</a></li>
                      <li><a href="#">4</a></li>
                      <li><a href="#">5</a></li>
                      <li><a href="#">6</a></li>
                      <li><a href="#"><i class="fa fa-chevron-right"></i></a></li>
                  </ul> --}}
          </div>
         </div>
      </section>
@stop