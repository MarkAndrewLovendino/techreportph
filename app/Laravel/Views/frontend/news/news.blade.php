 @extends('frontend._layout.main')
@section('content')
      <div class="clearfix"></div>
      <!-- BREAADCRUMB SECTION -->
      <section class="my-breadcrumb parallex">
         <div class="container page-banner">
            <div class="row">
               <div class="col-sm-6 col-md-6">
                  <h1>NEWS</h1>
               </div>
            <div class="col-sm-6 col-md-6">
                  <ol class="breadcrumb pull-right">
                     <li><a href="index-2.html">Home</a></li>
                     <li><a href="#">News</a></li>
                  </ol>
               </div>
            </div>
         </div>
      </section>

     <section class="blog-posts">
         <div class="container">
            <div class="row">
               
               <div class="col-md-8 col-sm-12 col-xs-12">    
               @foreach($news as $index => $info)    
                  <div class="single_stuff wow fadeInDown">
                    <div class="single_stuff_img"> <a href="pages/single.html"><img src="{{asset($info->directory.'/'.$info->filename)}}" alt=""></a> </div>
                    <div class="single_stuff_article">
                      <div class="single_sarticle_inner"> <a class="stuff_category" href="#">Technology</a>
                        <div class="stuff_article_inner"> <span class="stuff_date"><center>{{Helper::date_format($info->posted_at,'F')}}</center><strong>{{Helper::date_format($info->posted_at,'d')}}</strong></span>
                           <h2 class="post-title"><a href="{{route('frontend.news.show',$info->id.'-'.Str::slug($info->title))}}">{{$info->title}}</a></h2>
                          <p>{!!Str::limit(strip_tags($info->content),$limit = 300)!!}</p>
                        </div>
                      </div>
                    </div>
                  </div>
                   @endforeach
                    <div class="pager">
                        {!!$news->appends(Input::except('page'))->render()!!}
                        {{-- <ul class="pagination style2">
                          <li><a href="#"><i class="fa fa-chevron-left"></i></a></li>
                          <li class="selected"><a href="#">1</a></li>
                          <li><a href="#">2</a></li>
                          <li><a href="#">3</a></li>
                          <li><a href="#">4</a></li>
                          <li><a href="#">5</a></li>
                          <li><a href="#">6</a></li>
                          <li><a href="#"><i class="fa fa-chevron-right"></i></a></li>
                        </ul> --}}
                    </div>
               </div>

               <div class="col-md-4 col-sm-4 col-xs-12">
                  <aside>
                     <div class="widget">
                        <div class="search">
                          <form action="" method="get">
                           <div class="input-group stylish-input-group">
                              <input type="text" name="keyword" id="t2-search" class="form-control" placeholder="Enter search keyword..." value="{{Input::get('keyword')}}" />
                              <span class="input-group-addon">
                              <button type="submit" class="main-color"><i class="fa fa-search"></i></button>
                              </span> 
                           </div>
                          </form>
                        </div>
                     </div>   
                    
                    <div class="widget">
                        <h4><b>Category</b></h4>
                        <ul class="categories-module">
                          @foreach($category as $index => $info)
                            <li> <a href="{{route('frontend.news.news',$info->id.'-'.Str::slug($info->title))}}">{{$info->title}}<span>({{count($info->title)}})</span> </a> </li>
                          @endforeach
                        </ul>
                     </div>

                     <div class="widget">
                        <h4>Recent Blogs</h4>
                        @foreach($news as $index => $info)
                        <div class="single-pp-wrapper">
                           <div class="popular-post-img"> <a href="#"><img src="{{asset($info->directory.'/'.$info->filename)}}" alt=""></a> </div>
                           <div class="popular-post-content">
                              <h5 class="post-title"><a href="{{route('frontend.news.show',$info->id.'-'.Str::slug($info->title))}}">{{$info->title}}</a></h5>
                              <p>On {{Helper::date_format($info->posted_at,'F d, Y')}}</p>
                           </div>
                        </div>
                        @endforeach
                     </div>
                  </aside>

               </div>
            </div>
         </div>
      </section>
@stop


