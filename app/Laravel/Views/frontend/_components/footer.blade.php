      <footer class="footer">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-sm-12 col-xs-12 nopadding">
                  <div class="col-md-4 col-sm-12 col-xs-12">
                     <div class="footer_block">
                        <a href="index-2.html" class="f_logo">
                        <img src="{{asset('frontend/images/techreport2.png')}}" class="img-responsive" alt="logo">
                        </a>
                        <p>{{str_limit(strip_tags($about->about), $limit = 200)}}.</p>
                     </div>
                    <div class="social-bar">
                         <ul>
                           <li><a class="fa fa-twitter" href="#"></a></li>
                           <li><a class="fa fa-pinterest" href="#"></a></li>
                           <li><a class="fa fa-facebook" href="#"></a></li>
                           <li><a class="fa fa-behance" href="#"></a></li>
                           <li><a class="fa fa-instagram" href="#"></a></li>
                           <li><a class="fa fa-linkedin" href="#"></a></li>
                        </ul>
                     </div>
                  </div>
                  <div class="col-md-4 col-sm-6 col-xs-12">
                     <div class="footer_block">
                        <h4 class="f-h-padding">Hot Links</h4>
                        <ul class="footer-links">
                           <li> <a href="#">Home</a> </li>
                           <li> <a href="#">About Us</a> </li>
                           <li> <a href="#">Privacy</a> </li>
                           <li> <a href="#">Contact Us</a> </li>
                           <li> <a href="#">Term &amp; Conditions</a> </li>
                        </ul>
                        <ul class="footer-links">
                           <li> <a href="#">Seo Packages</a> </li>
                           <li> <a href="#">Our Pricing</a> </li>
                           <li> <a href="#">FAQ's</a> </li>
                           <li> <a href="#">Contact Us</a> </li>
                           <li> <a href="#">Our Clients</a> </li>
                        </ul>
                     </div>
                  </div>
                  <div class="col-md-4 col-sm-6 col-xs-12">
                     <div class="footer_block">
                        <h4>Contact Information</h4>
                        <ul class="personal-info">
                           <li><i class="fa fa-map-marker"></i>{{$about->address}}.</li>
                           <li><i class="fa fa-envelope"></i>{{$about->email}}</li>
                           <li><i class="fa fa-phone"></i> {{$about->contact}}</li>
                           <li><i class="fa fa-clock-o"></i> Mon - Sun: 8:00 - 16:00</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </footer>
      <div class="footer-bottom text-center">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-sm-12 col-xs-12 nopadding">
                  <div class="col-md-12 col-sm-12 col-xs-12">
                     <p>&copy; Copyright 2018  <a href="">TECHREPORT PH</a> - All Rights Reserved.</p>
                  </div>
               </div>
            </div>
         </div>
      </div>