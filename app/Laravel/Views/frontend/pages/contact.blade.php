@extends('frontend._layout.main')
@section('content')
<div class="clearfix"></div>
      <!-- BREAADCRUMB SECTION -->
      <section class="my-breadcrumb parallex">
         <div class="container page-banner">
            <div class="row">
               <div class="col-sm-6 col-md-6">
                  <h1>CONTACT US</h1>
               </div>
            </div>
         </div>
      </section>
      <section class="contact-us">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-sm-12 col-xs-12 heading">
                  <div class="main-heading-container">
                     <h1>GET IN TOUCH</h1>
                     <h4>Please provide your details and we will contact you as soon as possible.</h4>
                  </div>
               </div>
               <div class="col-md-12 col-sm-12 col-xs-12 nopadding">
                  <div class="col-md-8 col-sm-7 col-xs-12">
                     <form id="target" class="row" action="" method="POST" enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <div class="col-md-6 col-sm-12">
                           <div class="form-group">
                              <label>Your Name <span class="required">*</span></label>
                              <input name = "name" placeholder="" class="form-control" type="text">
                           </div>
                        </div>
                        <div class="col-md-6 col-sm-12">
                           <div class="form-group">
                              <label>Your Email Address <span class="required">*</span></label>
                              <input name = "email" placeholder="" class="form-control" type="company">
                           </div>
                        </div>
                        <div class="col-md-6 col-sm-12">
                           <div class="form-group">
                              <label>Phone <span class="required">*</span></label>
                              <input name = "contact" placeholder="" class="form-control" type="company">
                           </div>
                        </div>
                        <div class="col-md-6 col-sm-12">
                           <div class="form-group">
                              <label>Subject<span class="required">*</span></label>
                              <input name = "subject" placeholder="" class="form-control" type="company">
                           </div>
                        </div>
                        <div class="col-md-12 col-sm-12">
                           <div class="form-group">
                              <label>Message <span class="required">*</span></label>
                              <textarea name = "message" cols="6" rows="8" placeholder="" class="form-control"></textarea>
                           </div>
                        </div>
                        <div class="col-md-12 col-sm-12">
                            <button class="btn btn-custom" id="save" type="submit" data-loading-text="<i class='icon-spinner2 spinner position-left'></i> Saving ..." class="btn btn-primary btn-raised btn-lg btn-loading">Send Message</button>
                        </div>
                     </form>
                  </div>
                  <div class="col-md-4 col-sm-5 col-xs-12">
                     <div class="services-grid-3">
                        <i class="icon ti-location-arrow"></i>
                        <div class="content-area">
                           <h4>Contact Detail</h4>
                           <p> <span class ="glyphicon glyphicon-earphone"></span>  +92 333 123 4567</p>
                           <p><span><span class="glyphicon glyphicon-envelope"></span> Email:</span>Support@admin.com</p>
                        </div>
                     </div><hr>
                     <div class="services-grid-3">
                        <i class="icon ti-location-arrow"></i>
                        <div class="content-area">
                           <h4>Address</h4>
                           <p><span class="glyphicon glyphicon-map-marker"></span> 3rd Floor,Link Arcade Model Town, BBL, USA.</p>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
@stop