@extends('frontend._layout.main')
@section('content')
<div class="clearfix"></div>
   <!-- BREAADCRUMB SECTION -->
      <section class="my-breadcrumb parallex">
         <div class="container page-banner">
            <div class="row">
               <div class="col-sm-6 col-md-6">
                  <h1>FAQs</h1>
                  <h4>Frequently Asked Questions</h4>
               </div>
            </div>
         </div>
      </section>
      <section class="contact-us">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-sm-12 col-xs-12 heading">
                  <div class="main-heading-container">
                     <h1>Ask Your Question</h1>
                  </div>
               </div>
               <div class="col-md-12 col-sm-12 col-xs-12 nopadding">
                  <div class="col-md-8 col-sm-7 col-xs-12">
                     <form id="target" class="row" action="" method="POST" enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <div class="col-md-6 col-sm-12">
                           <div class="form-group">
                              <label>Name <span class="required">*</span></label>
                              <input id = "name" name= "name" placeholder="" class="form-control" type="text">
                           </div>
                        </div>
                        <div class="col-md-6 col-sm-12">
                           <div class="form-group">
                              <label>Email Address <span class="required">*</span></label>
                              <input id="email" name = "email" placeholder="" class="form-control" type="company">
                           </div>
                        </div>
                        <div class="col-md-12 col-sm-12">
                           <div class="form-group">
                              <label>Ask a Question <span class="required">*</span></label>
                              <textarea cols="6" rows="8" id = "question" name = "question" placeholder="" class="form-control"></textarea>
                           </div>
                        </div>
                        <div class="col-md-12 col-sm-12">
                           <button id="save" type="submit" data-loading-text="<i class='icon-spinner2 spinner position-left'></i> Saving ..." class="btn btn-primary btn-raised btn-lg btn-loading">Save</button>
                        </div>

                     </form>
                  </div>
                  <div class="col-md-4 col-sm-5 col-xs-12">
                     <div class="services-grid-3">
                        <i class="icon ti-location-arrow"></i>
                        <div class="content-area">
                           <h4>Contact Detail</h4>
                           <p><span class ="glyphicon glyphicon-earphone"></span> +92 333 123 4567</p>
                           <p><span class="glyphicon glyphicon-envelope"></span><span>Email:</span>Support@admin.com</p>
                        </div>
                     </div><hr>
                     <div class="services-grid-3">
                        <i class="icon ti-location-arrow"></i>
                        <div class="content-area">
                           <h4>Address</h4>
                           <p><span class="glyphicon glyphicon-map-marker"></span> 3rd Floor,Link Arcade Model Town, BBL, USA.</p>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>   
@stop