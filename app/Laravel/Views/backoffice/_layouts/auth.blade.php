<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from demo.interface.club/limitless/layout_1/LTR/material/login_tabbed.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 30 May 2016 06:08:39 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>{{env('APP_TITLE','Localhost')}} - CPanel Login</title>

	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="{{asset('backoffice/css/icons/icomoon/styles.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('backoffice/css/bootstrap.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('backoffice/css/core.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('backoffice/css/components.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('backoffice/css/colors.css')}}" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	@yield('page-styles')

	<!-- Core JS files -->
	<script type="text/javascript" src="{{asset('backoffice/js/plugins/loaders/pace.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('backoffice/js/core/libraries/jquery.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('backoffice/js/core/libraries/bootstrap.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('backoffice/js/plugins/loaders/blockui.min.js')}}"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	@yield('page-scripts')

	<script type="text/javascript" src="{{asset('backoffice/js/plugins/forms/styling/uniform.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('backoffice/js/core/app.js')}}"></script>
	<script type="text/javascript" src="{{asset('backoffice/js/pages/login.js')}}"></script>

	<script type="text/javascript" src="{{asset('backoffice/js/plugins/ui/ripple.min.js')}}"></script>
	<!-- /theme JS files -->

</head>

<body class="login-container login-cover">

	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Content area -->
				<div class="content pb-20">

					@yield('content')

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->

</body>

<!-- Mirrored from demo.interface.club/limitless/layout_1/LTR/material/login_tabbed.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 30 May 2016 06:08:39 GMT -->
</html>
