<?php namespace App\Laravel\Controllers\Backoffice;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\Awards;

/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\Backoffice\AwardsRequest;

/**
*
* Classes used for this controller
*/
use App\Http\Requests\Request;
use Input, Helper, Carbon, Session, Str, File, Image, ImageUploader;

class AwardsController extends Controller{

	/**
	*
	* @var Array $data
	*/
	protected $data;

	public function __construct () {
		parent::__construct();
		$view = Input::get('view','table');
		array_merge($this->data, parent::get_data());
		$this->data['page_title'] = "Awards";
		$this->data['page_description'] = "This is the general information about ".$this->data['page_title'].".";
		$this->data['route_file'] = "awards";
	}

	public function index () {
		$this->data['awards'] = Awards::orderBy('created_at',"DESC")->get();
		return view('backoffice.'.$this->data['route_file'].'.index',$this->data);
	}

	public function create () {
		return view('backoffice.'.$this->data['route_file'].'.create',$this->data);
	}

	public function store (AwardsRequest $request) {
		try {
			$new_awards = new Awards;
			$new_awards->fill($request->all());

			if($request->hasFile('file')){
				$upload = ImageUploader::upload($request->file,'storage/awards');
				$new_awards->path = $upload["path"];
				$new_awards->directory = $upload["directory"];
				$new_awards->filename = $upload["filename"];
			}

			if($new_awards->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"New social link has been added.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

			return redirect()->back();
		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function edit ($id = NULL) {
		$awards = Awards::find($id);

		if (!$awards) {
			Session::flash('notification-status',"failed");
			Session::flash('notification-msg',"Record not found.");
			return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
		}

		$this->data['social_links'] = $awards;
		return view('backoffice.'.$this->data['route_file'].'.edit',$this->data);
	}

	public function update (AwardsRequest $request, $id = NULL) {
		try {
			$awards = Awards::find($id);

			if (!$awards) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			$awards->fill($request->all());

			if($request->hasFile('file')){
				$upload = ImageUploader::upload($request->file,'storage/social_links');
				if($upload){	
					if (File::exists("{$awards->directory}/{$awards->filename}")){
						File::delete("{$awards->directory}/{$awards->filename}");
					}
					if (File::exists("{$awards->directory}/resized/{$awards->filename}")){
						File::delete("{$awards->directory}/resized/{$awards->filename}");
					}
					if (File::exists("{$awards->directory}/thumbnails/{$awards->filename}")){
						File::delete("{$awards->directory}/thumbnails/{$awards->filename}");
					}
				}
				
				$awards->path = $upload["path"];
				$awards->directory = $upload["directory"];
				$awards->filename = $upload["filename"];
			}

			if($awards->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A social link has been updated.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function destroy ($id = NULL) {
		try {
			$awards = Awards::find($id);

			if (!$awards) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			if (File::exists("{$awards->directory}/{$awards->filename}")){
				File::delete("{$awards->directory}/{$awards->filename}");
			}
			if (File::exists("{$awards->directory}/resized/{$awards->filename}")){
				File::delete("{$awards->directory}/resized/{$awards->filename}");
			}
			if (File::exists("{$awards->directory}/thumbnails/{$awards->filename}")){
				File::delete("{$awards->directory}/thumbnails/{$awards->filename}");
			}

			if($awards->save() AND $awards->delete()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A social link has been deleted.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

}