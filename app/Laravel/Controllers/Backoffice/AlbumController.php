<?php namespace App\Laravel\Controllers\Backoffice;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\Album;

/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\Backoffice\AlbumRequest;
use App\Laravel\Requests\Backoffice\EditAlbumRequest;

/**
*
* Classes used for this controller
*/
use App\Http\Requests\Request;
use Input, Helper, Carbon, Session, Str, File, Image, ImageUploader;

class AlbumController extends Controller{

	/**
	*
	* @var Array $data
	*/
	protected $data;

	public function __construct () {
		parent::__construct();
		$view = Input::get('view','table');
		array_merge($this->data, parent::get_data());
		$this->data['page_title'] = "Album";
		$this->data['page_description'] = "This is the general information about ".$this->data['page_title'].".";
		$this->data['route_file'] = "albums";
	}

	public function index () {
		$this->data['albums'] = Album::orderBy('created_at',"DESC")->get();
		return view('backoffice.'.$this->data['route_file'].'.index',$this->data);
	}

	public function create () {
		return view('backoffice.'.$this->data['route_file'].'.create',$this->data);
	}

	public function store (AlbumRequest $request) {
		try {
			$new_album = new Album;
			$new_album->fill($request->all());

			if($request->hasFile('file')){
				$upload = ImageUploader::upload($request->file,'storage/album');
				$new_album->path = $upload["path"];
				$new_album->directory = $upload["directory"];
				$new_album->filename = $upload["filename"];
			}

			if($new_album->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"New album has been added.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

			return redirect()->back();
		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function edit ($id = NULL) {
		$albums = Album::find($id);

		if (!$albums) {
			Session::flash('notification-status',"failed");
			Session::flash('notification-msg',"Record not found.");
			return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
		}

		$this->data['album'] = $albums;
		return view('backoffice.'.$this->data['route_file'].'.edit',$this->data);
	}

	public function update (EditAlbumRequest $request, $id = NULL) {
		try {
			$albums = Album::find($id);

			if (!$albums) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			$albums->fill($request->all());

			if($request->hasFile('file')){
				$upload = ImageUploader::upload($request->file,'storage/album');
				if($upload){	
					if (File::exists("{$albums->directory}/{$albums->filename}")){
						File::delete("{$albums->directory}/{$albums->filename}");
					}
					if (File::exists("{$albums->directory}/resized/{$albums->filename}")){
						File::delete("{$albums->directory}/resized/{$albums->filename}");
					}
					if (File::exists("{$albums->directory}/thumbnails/{$albums->filename}")){
						File::delete("{$albums->directory}/thumbnails/{$albums->filename}");
					}
				}
				
				$albums->path = $upload["path"];
				$albums->directory = $upload["directory"];
				$albums->filename = $upload["filename"];
			}

			if($albums->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A album has been updated.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function destroy ($id = NULL) {
		try {
			$albums = Album::find($id);

			if (!$albums) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			if (File::exists("{$albums->directory}/{$albums->filename}")){
				File::delete("{$albums->directory}/{$albums->filename}");
			}
			if (File::exists("{$albums->directory}/resized/{$albums->filename}")){
				File::delete("{$albums->directory}/resized/{$albums->filename}");
			}
			if (File::exists("{$albums->directory}/thumbnails/{$albums->filename}")){
				File::delete("{$albums->directory}/thumbnails/{$albums->filename}");
			}

			if($albums->save() AND $albums->delete()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A album has been deleted.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

}