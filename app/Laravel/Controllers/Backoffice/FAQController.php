<?php namespace App\Laravel\Controllers\Backoffice;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\FAQ;

/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\Backoffice\FAQRequest;
use App\Laravel\Requests\Backoffice\EditFAQRequest;

/**
*
* Classes used for this controller
*/
use App\Http\Requests\Request;
use Input, Helper, Carbon, Session, Str, File, Image, ImageUploader;

class FAQController extends Controller{

	/**
	*
	* @var Array $data
	*/
	protected $data;

	public function __construct () {
		parent::__construct();
		$view = Input::get('view','table');
		array_merge($this->data, parent::get_data());
		$this->data['page_title'] = "Frequently Asked Question";
		$this->data['page_description'] = "This is the general information about ".$this->data['page_title'].".";
		$this->data['route_file'] = "faq";
	}

	public function index () {
		$this->data['faq'] = FAQ::orderBy('created_at',"DESC")->get();
		return view('backoffice.'.$this->data['route_file'].'.index',$this->data);
	}

	public function create () {
		return view('backoffice.'.$this->data['route_file'].'',$this->data);
	}

	public function store (FAQRequest $request) {
		try {
			$new_faq = new FAQ;
			$new_faq->fill($request->all());

			if($new_faq->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"New faq has been added.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

			return redirect()->back();
		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function edit ($id = NULL) {
		$faq = FAQ::find($id);

		if (!$faq) {
			Session::flash('notification-status',"failed");
			Session::flash('notification-msg',"Record not found.");
			return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
		}

		$this->data['faq'] = $faq;
		return view('backoffice.'.$this->data['route_file'].'.edit',$this->data);
	}

	public function update (EditFAQRequest $request, $id = NULL) {
		try {
			$faq = FAQ::find($id);

			if (!$faq) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			$faq->fill($request->all());

			if($faq->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A faq has been updated.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function destroy ($id = NULL) {
		try {
			$faq = FAQ::find($id);

			if (!$faq) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			if (File::exists("{$faq->directory}/{$faq->filename}")){
				File::delete("{$faq->directory}/{$faq->filename}");
			}
			if (File::exists("{$faq->directory}/resized/{$faq->filename}")){
				File::delete("{$faq->directory}/resized/{$faq->filename}");
			}
			if (File::exists("{$faq->directory}/thumbnails/{$faq->filename}")){
				File::delete("{$faq->directory}/thumbnails/{$faq->filename}");
			}

			if($faq->save() AND $faq->delete()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A faq has been deleted.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

}