<?php namespace App\Laravel\Controllers\Backoffice;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\Event;

/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\Backoffice\EventRequest;
use App\Laravel\Requests\Backoffice\EditEventRequest;

/**
*
* Classes used for this controller
*/
use App\Http\Requests\Request;
use Input, Helper, Carbon, Session, Str, File, Image, ImageUploader;

class EventController extends Controller{

	/**
	*
	* @var Array $data
	*/
	protected $data;

	public function __construct () {
		parent::__construct();
		$view = Input::get('view','table');
		array_merge($this->data, parent::get_data());
		$this->data['page_title'] = "Event";
		$this->data['page_description'] = "This is the general information about ".$this->data['page_title'].".";
		$this->data['route_file'] = "events";
	}

	public function index () {
		$this->data['events'] = Event::orderBy('created_at',"DESC")->get();
		return view('backoffice.'.$this->data['route_file'].'.index',$this->data);
	}

	public function create () {
		return view('backoffice.'.$this->data['route_file'].'.create',$this->data);
	}

	public function store (EventRequest $request) {
		try {
			$new_event = new Event;
			$new_event->fill($request->all());

			$date = explode(' - ',$request->get('daterange'));
			$start = Helper::date_format($date[0],'Y-m-d H:i');
			// $end = Helper::date_format($date[1],'Y-m-d H:i');

			$new_event->start = $start;
			// $new_event->end = $end;

			if($request->hasFile('file')){
				$upload = ImageUploader::upload($request->file,'storage/event');
				$new_event->path = $upload["path"];
				$new_event->directory = $upload["directory"];
				$new_event->filename = $upload["filename"];
			}

			if($new_event->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"New event has been added.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

			return redirect()->back();
		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function edit ($id = NULL) {
		$event = Event::find($id);

		if (!$event) {
			Session::flash('notification-status',"failed");
			Session::flash('notification-msg',"Record not found.");
			return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
		}

		$this->data['event'] = $event;
		return view('backoffice.'.$this->data['route_file'].'.edit',$this->data);
	}

	public function update (EditEventRequest $request, $id = NULL) {
		try {
			$event = Event::find($id);

			$date = explode(' - ',$request->get('daterange'));
			$start = Helper::date_format($date[0],'Y-m-d H:i');
			$end = Helper::date_format($date[1],'Y-m-d H:i');

			$event->start = $start;
			$event->end = $end;

			if (!$event) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			$event->fill($request->all());

			if($request->hasFile('file')){
				$upload = ImageUploader::upload($request->file,'storage/event');
				if($upload){	
					if (File::exists("{$event->directory}/{$event->filename}")){
						File::delete("{$event->directory}/{$event->filename}");
					}
					if (File::exists("{$event->directory}/resized/{$event->filename}")){
						File::delete("{$event->directory}/resized/{$event->filename}");
					}
					if (File::exists("{$event->directory}/thumbnails/{$event->filename}")){
						File::delete("{$event->directory}/thumbnails/{$event->filename}");
					}
				}
				
				$event->path = $upload["path"];
				$event->directory = $upload["directory"];
				$event->filename = $upload["filename"];
			}

			if($event->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A event has been updated.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function destroy ($id = NULL) {
		try {
			$event = Event::find($id);

			if (!$event) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			if (File::exists("{$event->directory}/{$event->filename}")){
				File::delete("{$event->directory}/{$event->filename}");
			}
			if (File::exists("{$event->directory}/resized/{$event->filename}")){
				File::delete("{$event->directory}/resized/{$event->filename}");
			}
			if (File::exists("{$event->directory}/thumbnails/{$event->filename}")){
				File::delete("{$event->directory}/thumbnails/{$event->filename}");
			}

			if($event->save() AND $event->delete()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A event has been deleted.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

}