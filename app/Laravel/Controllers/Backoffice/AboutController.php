<?php namespace App\Laravel\Controllers\Backoffice;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\About;

/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\Backoffice\AboutRequest;

/**
*
* Classes used for this controller
*/
use App\Http\Requests\Request;
use Input, Helper, Carbon, Session, Str, File, Image, ImageUploader;

class AboutController extends Controller{

	/**
	*
	* @var Array $data
	*/
	protected $data;

	public function __construct () {
		parent::__construct();
		$view = Input::get('view','table');
		array_merge($this->data, parent::get_data());
		$this->data['page_title'] = "About";
		$this->data['page_description'] = "This is the general information about ".$this->data['page_title'].".";
		$this->data['route_file'] = "about";
	}

	public function edit () {
		$this->data['about'] = About::orderBy('created_at',"DESC")->first() ? : new About;
		return view('backoffice.'.$this->data['route_file'].'.edit',$this->data);
	}

	public function update(AboutRequest $request){
		$about =About::orderBy('created_at',"DESC")->first() ? : new About;
		
		$about->about = Input::get('about');
		$about->address = Input::get('address');
		$about->email = Input::get('email');
		$about->contact = Input::get('contact');


		if($about->save()) {
			Session::flash('notification-status','success');
			Session::flash('notification-msg',"About has been updated.");
			return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
		}

		Session::flash('notification-status','failed');
		Session::flash('notification-msg','Something went wrong.');

		return redirect()->back();
	}

}