<?php namespace App\Laravel\Controllers\Backoffice;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\SocialLink;

/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\Backoffice\SocialLinkRequest;

/**
*
* Classes used for this controller
*/
use App\Http\Requests\Request;
use Input, Helper, Carbon, Session, Str, File, Image, ImageUploader;

class SocialLinkController extends Controller{

	/**
	*
	* @var Array $data
	*/
	protected $data;

	public function __construct () {
		parent::__construct();
		$view = Input::get('view','table');
		array_merge($this->data, parent::get_data());
		$this->data['page_title'] = "SocialLink";
		$this->data['page_description'] = "This is the general information about ".$this->data['page_title'].".";
		$this->data['route_file'] = "social_links";

		$this->data['types'] = [ '' => "Choose type", 'facebook' => "Facebook", 'twitter' => "Twitter",'youtube' => "Youtube",'pinterest' => "Pinterest",'instagram' => "Instagram",'google' => "Google",'google-plus' => "Google Plus", 'github'=>"Github",'wordpress'=>"Wordpress",'vimeo'=>"Vimeo",'skype'=>"Skype",'spotify'=>"Spotify"];
	}

	public function index () {
		$this->data['social_links'] = SocialLink::orderBy('created_at',"DESC")->get();
		return view('backoffice.'.$this->data['route_file'].'.index',$this->data);
	}

	public function create () {
		return view('backoffice.'.$this->data['route_file'].'.create',$this->data);
	}

	public function store (SocialLinkRequest $request) {
		try {
			$new_social_link = new SocialLink;
			$new_social_link->fill($request->all());

			if($request->hasFile('file')){
				$upload = ImageUploader::upload($request->file,'storage/social_links');
				$new_social_link->path = $upload["path"];
				$new_social_link->directory = $upload["directory"];
				$new_social_link->filename = $upload["filename"];
			}

			if($new_social_link->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"New social link has been added.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

			return redirect()->back();
		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function edit ($id = NULL) {
		$social_link = SocialLink::find($id);

		if (!$social_link) {
			Session::flash('notification-status',"failed");
			Session::flash('notification-msg',"Record not found.");
			return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
		}

		$this->data['social_links'] = $social_link;
		return view('backoffice.'.$this->data['route_file'].'.edit',$this->data);
	}

	public function update (SocialLinkRequest $request, $id = NULL) {
		try {
			$social_link = SocialLink::find($id);

			if (!$social_link) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			$social_link->fill($request->all());

			if($request->hasFile('file')){
				$upload = ImageUploader::upload($request->file,'storage/social_links');
				if($upload){	
					if (File::exists("{$social_link->directory}/{$social_link->filename}")){
						File::delete("{$social_link->directory}/{$social_link->filename}");
					}
					if (File::exists("{$social_link->directory}/resized/{$social_link->filename}")){
						File::delete("{$social_link->directory}/resized/{$social_link->filename}");
					}
					if (File::exists("{$social_link->directory}/thumbnails/{$social_link->filename}")){
						File::delete("{$social_link->directory}/thumbnails/{$social_link->filename}");
					}
				}
				
				$social_link->path = $upload["path"];
				$social_link->directory = $upload["directory"];
				$social_link->filename = $upload["filename"];
			}

			if($social_link->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A social link has been updated.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function destroy ($id = NULL) {
		try {
			$social_link = SocialLink::find($id);

			if (!$social_link) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			if (File::exists("{$social_link->directory}/{$social_link->filename}")){
				File::delete("{$social_link->directory}/{$social_link->filename}");
			}
			if (File::exists("{$social_link->directory}/resized/{$social_link->filename}")){
				File::delete("{$social_link->directory}/resized/{$social_link->filename}");
			}
			if (File::exists("{$social_link->directory}/thumbnails/{$social_link->filename}")){
				File::delete("{$social_link->directory}/thumbnails/{$social_link->filename}");
			}

			if($social_link->save() AND $social_link->delete()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A social link has been deleted.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

}