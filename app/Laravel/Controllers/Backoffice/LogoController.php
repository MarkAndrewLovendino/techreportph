<?php namespace App\Laravel\Controllers\Backoffice;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\Logo;

/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\Backoffice\LogoRequest;

/**
*
* Classes used for this controller
*/
use App\Http\Requests\Request;
use Input, Helper, Carbon, Session, Str, File, Image, ImageUploader;

class LogoController extends Controller{

	/**
	*
	* @var Array $data
	*/
	protected $data;

	public function __construct () {
		parent::__construct();
		$view = Input::get('view','table');
		array_merge($this->data, parent::get_data());
		$this->data['page_title'] = "Logo";
		$this->data['page_description'] = "This is the general information about ".$this->data['page_title'].".";
		$this->data['route_file'] = "logo";
	}

	public function edit () {
		$this->data['logo'] = Logo::orderBy('created_at',"DESC")->first() ? : new Logo;
		return view('backoffice.'.$this->data['route_file'].'.edit',$this->data);
	}

	public function update(LogoRequest $request){
		$logo = Logo::orderBy('created_at',"DESC")->first() ? : new Logo;

		if($request->hasFile('file')){
			$upload = ImageUploader::upload($request->file,'storage/logo');
			$logo->path = $upload["path"];
			$logo->directory = $upload["directory"];
			$logo->filename = $upload["filename"];
		}

		if($logo->save()) {
			Session::flash('notification-status','success');
			Session::flash('notification-msg',"Logo has been updated.");
			return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
		}

		Session::flash('notification-status','failed');
		Session::flash('notification-msg','Something went wrong.');

		return redirect()->back();
	}

}