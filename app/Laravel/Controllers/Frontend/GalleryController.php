<?php 

namespace App\Laravel\Controllers\Frontend;

/*
*
* Models used for this controller
*/
use App\User;
use App\Laravel\Models\NewsCategory;
use App\Laravel\Models\News;

/*
*
* Requests used for validating inputs
*/


/*
*
* Classes used for this controller
*/
use Helper, Carbon, Session, Str, DB;

class GalleryController extends Controller{

	/*
	*
	* @var Array $data
	*/
	protected $data;

	public function __construct () {
		$this->data = [];
		parent::__construct();
		array_merge($this->data, parent::get_data());
	}

	public function index () {
		$this->data['albums'] = NewsCategory::orderBy('created_at','DESC')->paginate(9);
		return view('frontend.gallery.index',$this->data);
	}

	public function show ($id = NULL) {
		$this->data['album'] = NewsCategory::find($id);
		$this->data['images'] = News::where('category',$id)->paginate(3);
		
		return view('frontend.gallery.show',$this->data);
	}
}