<?php 

namespace App\Laravel\Controllers\Frontend;

/*
*
* Models used for this controller
*/
use App\User;
use App\Laravel\Models\Member;
use App\Laravel\Models\FAQ;
use App\Laravel\Models\ContactInquiry;
use App\Laravel\Models\Subscriber;


use App\Laravel\Requests\Frontend\FAQRequest;

/*
*
* Requests used for validating inputs
*/

use App\Laravel\Requests\Frontend\ContactInquiryRequest;
use App\Laravel\Events\SendEmail;

/*
*
* Classes used for this controller
*/
use Helper, Carbon, Session, Str, DB, Input, Event;

class PageController extends Controller{

	/*
	*
	* @var Array $data
	*/
	protected $data;

	public function __construct () {
		$this->data = [];
		parent::__construct();
		array_merge($this->data, parent::get_data());
	}

	public function index () {
		return view('frontend.index',$this->data);
	}

	public function about () {
		return view('frontend.pages.about',$this->data);
	}

	public function contact () {
		return view('frontend.pages.contact',$this->data);
	}

	public function faq () {
		return view('frontend.pages.faq',$this->data);
	}

	public function faqstore (FAQRequest $request) {
		try {
			$new_faq = new FAQ;
			$new_faq->fill($request->all());

			if($new_faq->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"New faq has been added.");
				return redirect()->route('frontend.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

			return redirect()->back();
		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function contactstore (ContactInquiryRequest $request) {
		try {
			$new_contact = new ContactInquiry;
			$new_contact->fill($request->all());

			if($new_contact->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"New contact has been added.");
				return redirect()->route('frontend.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

			return redirect()->back();
		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

}