<?php 

namespace App\Laravel\Controllers\Frontend;

use App\Laravel\Models\About;   
use App\Laravel\Models\ImageSlider;
use App\Laravel\Models\Logo;
use App\Laravel\Models\News;
use App\Laravel\Models\Testimonial;
use App\Laravel\Models\Partner;
use App\Laravel\Models\NewsCategory;

use Illuminate\Support\Collection;
use App\Http\Controllers\Controller as MainController;
use Auth, Session,Carbon, Helper,Route,DNS2D;

class Controller extends MainController{

	protected $data;

	public function __construct(){
		self::set_about();
		self::set_imageslider();
		self::set_logo();
		self::set_news();
		self::set_testimonials();
		self::set_partners();
		self::set_partners_1();
		self::set_category();

	}	

	public function get_data(){
		return $this->data;
	}

	public function set_about(){
		$this->data['about'] = About::orderBy('created_at','DESC')->first()? : new About;
	}

	public function set_imageslider(){
				$this->data['slider'] = ImageSlider::orderBy('order','ASC')->get();
	}

	public function set_logo(){
			$this->data['logo'] = Logo::orderBy('created_at','DESC')->first()? : new Logo;
	}

	public function set_news(){
		$this->data['news'] = News::orderBy('created_at','DESC')->paginate(6);
	}

	public function set_testimonials(){
		$this->data['testimonials'] = Testimonial::orderBy('created_at','DESC')->get();
	}

	public function set_partners(){
		$this->data['partners'] = Partner::orderBy('created_at','DESC')->paginate(3);
	}

	public function set_partners_1(){
		$this->data['partners_1'] = Partner::orderBy('created_at','DESC')->get();
	}

	public function set_category(){
		$this->data['category'] = NewsCategory::orderBy('created_at',"DESC")->get();
	}

 

}