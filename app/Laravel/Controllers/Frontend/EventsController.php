<?php 

namespace App\Laravel\Controllers\Frontend;

/*
*
* Models used for this controller
*/
use App\User;
use App\Laravel\Models\Event;

/*
*
* Requests used for validating inputs
*/


/*
*
* Classes used for this controller
*/
use Helper, Carbon, Session, Str, DB, Calendar;

class EventsController extends Controller{

	/*
	*
	* @var Array $data
	*/
	protected $data;

	public function __construct () {
		$this->data = [];
		parent::__construct();
		array_merge($this->data, parent::get_data());
	}

	public function index () {
		$events = [];


			$schedules = Event::all();

			foreach($schedules as $index => $info){

				$item = Calendar::event(
					"{$info->title}",
					false,
					new \DateTime(Helper::date_format($info->start,"Y-m-d H:i")),
					new \DateTime(Helper::date_format($info->end,"Y-m-d H:i")), 
					"{$info->id}"
				);

				array_push($events, $item);
			}

		$this->data['calendar'] = Calendar::addEvents($events);
		// dd($this->data['calendar']->script()->options);
		$date_today = date(env('DATE_DB',"Y-m-d"),strtotime(Carbon::now()));

		$this->data['up_coming_events'] = Event::whereRaw("DATE(start) > '{$date_today}'")->paginate(4);
		return view('frontend.events.calendar',$this->data);
	}

	public function show ($id = NULL) {
		$this->data['event'] = Event::find($id);
		return view('frontend.events.show',$this->data);
	}
}